import { EventData, Page } from "tns-core-modules/ui/page/page";
import { LoginPageModel } from "./login-page-model";

let page: Page;

export function onLoaded(args: EventData) {
    page = args.object as Page;
    page.actionBarHidden = true;
    page.bindingContext = new LoginPageModel();
}