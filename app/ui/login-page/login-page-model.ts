import { Observable } from "tns-core-modules/ui/core/bindable/bindable";

export class LoginPageModel extends Observable {

    constructor() {
        super();
        this.set("appTitle", "Turystyczny Żywiec");
        this.set("appDescription", "Nazwa robocza");
        
        this.set("loginInfo", "Zaloguj się przy użyciu e-mail");
        this.set("loginHint", "Login");
        this.set("passwordHint", "Hasło");
        
        this.set("loginButton", "Zaloguj się");
    }
}