export class Point {
    constructor(public x = 0, public y = 0) { }
    
    /** @override */
    toString() {
        return `[x=${this.x},y=${this.y}]`;
    }
}
