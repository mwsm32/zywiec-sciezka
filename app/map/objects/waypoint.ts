import { Place } from "~/map/objects/place";
import { Point } from "~/map/objects/point";

export class Waypoint {
    id: number;
    location: Point;
    place: Place;
    active: boolean;
}
